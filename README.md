# Flectra Community / field-service

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[fieldservice](fieldservice/) | 2.0.1.1.1| Manage Field Service Locations, Workers and Orders
[fieldservice_timeline](fieldservice_timeline/) | 2.0.1.1.1| This module is a display timeline view of the Field Service order in Odoo, Flectra.
[fieldservice_skill](fieldservice_skill/) | 2.0.1.0.0| Manage your Field Service workers skills
[fieldservice_crm](fieldservice_crm/) | 2.0.1.0.0| Create Field Service orders from the CRM
[fieldservice_project](fieldservice_project/) | 2.0.1.0.0| Create field service orders from a project or project task
[base_territory](base_territory/) | 2.0.1.0.0| This module allows you to define territories, branches, districts and regions to be used for Field Service operations or Sales.
[fieldservice_account](fieldservice_account/) | 2.0.1.0.0| Track invoices linked to Field Service orders


