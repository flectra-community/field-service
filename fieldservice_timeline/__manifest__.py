# Copyright (C) 2021 Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Field Service Web Timeline",
    "summary": "This module is a display timeline view of the Field Service"
    " order in Odoo, Flectra.",
    "version": "2.0.1.1.1",
    "category": "Field Service",
    "license": "AGPL-3",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/field-service",
    "depends": ["fieldservice", "web_timeline"],
    "data": ["views/fsm_order.xml", "views/fsm_team.xml"],
    "qweb": ["static/src/xml/*.xml"],
    "development_status": "Beta",
    "maintainers": ["wolfhall", "max3903"],
    "uninstall_hook": "uninstall_hook",
}
